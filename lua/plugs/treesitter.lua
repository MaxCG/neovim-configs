return {
	{'nvim-treesitter/nvim-treesitter',
		lazy = false,
		dependencies = {
		},
		config = function()
			vim.cmd([[syntax manual]])
			local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
			parser_config.wgsl = {
				install_info = {
					url = "https://github.com/szebniok/tree-sitter-wgsl",
					files = { "src/parser.c" },
				}
			}
			parser_config.hypr = {
				install_info = {
					url = "https://github.com/luckasRanarison/tree-sitter-hypr",
					files = { "src/parser.c" },
					branch = "master",
				},
				filetype = "hypr",
			}
			require('nvim-treesitter.configs').setup {
				auto_install = true,
				sync_install = false,

				highlight = {
					enable = true,
					disable = function (lang, buff)
						disableList = {
							latex = function ()
								-- vim.bo.syntax = 'ON'
								return false
							end,
						}

						if disableList[lang] then
							return disableList[lang]()
						end

						return false
					end,
				},
				indent = {
					enable = true,
				},
				playground = {
					enable = false
				}
			}
		end
	},
	{'nvim-treesitter/playground',
		dependencies = {'nvim-treesitter/nvim-treesitter'},
		cmd = "TSPlaygroundToggle"
	},
	{'luckasRanarison/tree-sitter-hypr'},
	{'nvim-treesitter/nvim-treesitter-context'},
	{'szebniok/tree-sitter-wgsl'},
}
