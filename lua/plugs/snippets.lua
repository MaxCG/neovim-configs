local config = function()
	local ls = require('luasnip')

	ls.config.set_config {
		history = false,
		updateevents = 'TextChanged,TextChangedI',
		region_check_events = 'CursorMoved',
	}

	local function jump(x)
		if ls.jumpable(x) then
			ls.jump(x)
		end
	end

	noremap({'s', 'i'}, '<M-i>', function() jump(1) end)
	noremap({'s', 'i'}, '<M-h>', function() jump(-1) end)

	local s = ls.snippet
	local sn = ls.snippet_node
	local isn = ls.indent_snippet_node
	local t = ls.text_node
	local i = ls.insert_node
	local f = ls.function_node
	local c = ls.choice_node
	local d = ls.dynamic_node
	local r = ls.restore_node
	local events = req("luasnip.util.events")
	local ai = req("luasnip.nodes.absolute_indexer")
	local fmt = req("luasnip.extras.fmt").fmt
	local m = req("luasnip.extras").m
	local lambda = req("luasnip.extras").l
	local postfix = req("luasnip.extras.postfix").postfix

	local function snip(fts, ...)
		for _,ft in ipairs(fts) do
			ls.add_snippets(ft, {...})
		end
	end

	local function same(ind) return f(function(arg) return arg[1] end, { ind }) end

	-- Latex
	snip(
		{'tex', 'plaintex'},
		s('document', fmt([[
			\nofiles
			\documentclass{article}
			\usepackage[margin=2cm, a4paper]{geometry}
			\usepackage[utf8]{inputenc}
			\usepackage[T1]{fontenc}
			\usepackage[portuguese]{babel}

			\usepackage{amsmath}

			\usepackage{listings}
			\usepackage{xcolor}

			\usepackage{tikz}
			\usepackage{qtree}

			\usepackage{setspace}
			\usepackage{enumitem}
			\setlist{
				listparindent=\parindent
			}

			\newcommand{\leadingzero}[1]{\ifnum #1<<10 0\the#1\else\the#1\fi}
			\AtBeginDocument{
				\renewcommand{\today}{\leadingzero{\day}/\leadingzero{\month}/\the\year}
			}

			\lstnewenvironment{alg}[0] %defines the algorithm listing environment
			{
				\lstset{
					mathescape=true,
					keywordstyle=\color{black}\bfseries\em,
					keywords={,function,return,while,if,else,for,in,and,or,not},
					tabsize=3,
				}
			}
			{}

			\title{<>}
			\author{Maximilian Cabrajac Göritz\\NUSP: 11795418}

			\begin{document}
			\maketitle

			<>

			\end{document}
		]], {i(1), i(2)}, {delimiters = '<>'})),

		s('begin', fmt([[
			\begin{<>}
				<>
			\end{<>}
		]], {i(1), i(0), same(1)}, {delimiters = '<>'}))
	)
end

return {
	{'L3MON4D3/LuaSnip',
		lazy = true,
		config = config,
	},
}
