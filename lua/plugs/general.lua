return {
	{'numToStr/Comment.nvim',
		opts = {
			toggler = {
				line = '<M-/>',
			},
			opleader = {
				line = '<M-/>',
			}
		}
	},
	{'jiangmiao/auto-pairs',
		init = function ()
			vim.g.AutoPairsShortcutJump = ''
			vim.g.AutoPairsMultilineClose = 0
		end
	},
	{'ggandor/leap.nvim',
		lazy = true,
		init = function()
			map('', 's', function()
				local current_window = vim.fn.win_getid()
				require('leap').leap({ target_windows = { current_window } })
			end)
		end
	},
	{'mbbill/undotree',
		cmd = {"UndotreeToggle"},
		init = function()
			map('', '<leader>u', '<CMD>UndotreeToggle<CR>')
			vim.g.undotree_SetFocusWhenToggle = 1
			vim.opt.undofile = true
			vim.opt.backup = false
			vim.opt.writebackup = false
		end
	},
}
