return {
	{'ayu-theme/ayu-vim',
		lazy = false,
		config = function()
			vim.opt.termguicolors = true
			vim.g.ayucolor = 'dark'
			vim.cmd("colorscheme ayu")
		end
	},
	{'itchyny/lightline.vim',
		lazy = false,
		config = function()
			vim.opt.showmode = false
			vim.opt.laststatus = 2
			vim.g.lightline = {
				colorscheme = 'ayu_dark',
				mode_map = {
					n = ' N ',
					i = ' I ',
					v = ' V ',
					V = 'V L',
					["\22"] = 'V B',
					R = ' R ',
					c = ' C ',
					s = ' S ',
					S = 'S L',
					["\19"] = 'S B',
					t = ' T ',
				}
			}
		end
	},
}
