local config = function()
	local lspc = require('lspconfig')
	local cmplsp = require('cmp_nvim_lsp')

	local capabilities = vim.lsp.protocol.make_client_capabilities()
	capabilities = vim.tbl_deep_extend('force', capabilities, cmplsp.default_capabilities())

	vim.opt.signcolumn = 'number'

	local root_pattern = lspc.util.root_pattern

	local doc_hl_aug = vim.api.nvim_create_augroup('LspReferenceHighlight', { clear = true })

	vim.api.nvim_set_hl(0, 'LspReferenceText', {})
	vim.api.nvim_set_hl(0, 'LspReferenceRead', {bold = true})
	vim.api.nvim_set_hl(0, 'LspReferenceWrite', {bold = true, italic = true})

	local def = {
		capabilities = capabilities,
		on_attach = function (client, bufnr)
			local caps = client.server_capabilities

			if caps.definitionProvider then
				bufnoremap(bufnr, '', 'M', function()
					pos = vim.fn.getcurpos()
					vim.api.nvim_buf_set_mark(bufnr, 'm', pos[2], pos[3]-1, {})
					vim.lsp.buf.definition()
				end)
			end

			if caps.hoverProvider then
				bufnoremap(bufnr, '', 'mm', vim.lsp.buf.hover)
			end

			if caps.renameProvider then
				bufnoremap(bufnr, '', '<leader>c', vim.lsp.buf.rename)
			end

			if caps.referencesProvider then
				vim.opt_local.updatetime = 300
				vim.api.nvim_create_autocmd(
					{'CursorHold'}, {
						callback = function()
							vim.lsp.buf.document_highlight()
						end,
						buffer = bufnr, group = doc_hl_aug
				})
				vim.api.nvim_create_autocmd(
					{'CursorMoved'}, {
						callback = function()
							vim.lsp.buf.clear_references()
						end,
						buffer = bufnr, group = doc_hl_aug
				})
			end
		end
	}

	lspc.util.default_config = vim.tbl_deep_extend('force', lspc.util.default_config, def)

	local servers = {
		texlab = {},
		pyright = {},
		clangd = {
			root_dir = root_pattern(
						'main.cpp',
						'main.c',
						'.git'
					)
			,
		},
		ltex = {
			settings = {
				ltex = {
					language = 'pt'
				}
			}
		},
		rust_analyzer = {},
		arduino_language_server = {},
		hls = {},
		gopls = {},
		ts_ls = {},
	}

	for key, val in pairs(servers) do
		if (os.execute(string.format('command -v %s > /dev/null', lspc[key].document_config.default_config.cmd[1])) == 0) then
			lspc[key].setup(val)
		end
	end
end

return {
	{'neovim/nvim-lspconfig',
		dependencies = {
			'hrsh7th/cmp-nvim-lsp',
		},
		config = config,
	},

}
