local config = function()
	local cmp = require('cmp')
	local ls = require('luasnip')

	vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

	cmp.setup({
		snippet = {
			expand = function(args)
				ls.lsp_expand(args.body)
			end,
		},
		window = {
			documentation = cmp.config.window.bordered(),
		},
		mapping = {
			['<C-n>'] = cmp.mapping.select_next_item(),
			['<C-e>'] = cmp.mapping.select_prev_item(),
			['<M-n>'] = cmp.mapping.scroll_docs(4),
			['<M-e>'] = cmp.mapping.scroll_docs(-4),
			['<C-Space>'] = cmp.mapping.abort(),
			['<CR>'] = cmp.mapping.confirm({
				behavior = cmp.ConfirmBehavior.Insert,
				select = true
			}),
		},
		sources = cmp.config.sources(
			{
				{ name = 'luasnip' },
				{ name = 'nvim_lsp' },
				{ name = 'path' },
			},
			{
				-- { name = 'spell' }
			}
		),
		experimental = {
			ghost_text = true
		},
		formatting = {
			format = function (entry, vim_item)

				local menu = vim_item.menu

				if menu then
					-- remove rust's "(as TRAIT)"
					local as_trait = menu:match('^(%(as .-%))')
					vim.print(as_trait)
					if as_trait then
						menu = menu:sub(as_trait:len() + 1, menu:len())
					end

					-- chop if too long
					local max_len = 30
					if menu:len() > max_len then
						menu = menu:sub(0, max_len):match("^(.-)%s*$") .. "..."
					end
				end

				vim_item.menu = menu

				vim_item.kind = vim_item.kind:sub(0, 1)

				return vim_item
			end
		}
	})
end

return {
	{'hrsh7th/nvim-cmp',
		lazy = true,
		event = 'InsertEnter',
		dependencies = {
			'neovim/nvim-lspconfig',
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-path',
			'f3fora/cmp-spell',
			{'saadparwaiz1/cmp_luasnip',
				dependencies = {'L3MON4D3/LuaSnip'},
			},
		},
		config = config,
	},
}
