map = function(mode, lhs, rhs, opts)
	vim.keymap.set(mode, lhs, rhs, opts or {})
end

noremap = function(mode, lhs, rhs, opts)
	opts = opts or {}

	local options = { noremap = true }
	options = vim.tbl_extend("force", options, opts)

	map(mode, lhs, rhs, options)
end

bufmap = function(buf, mode, lhs, rhs, opts)
	opts = opts or {}

	local options = { buffer = buf }
	options = vim.tbl_extend("force", options, opts)

	map(mode, lhs, rhs, options)
end

bufnoremap = function(buf, mode, lhs, rhs, opts)
	opts = opts or {}

	local options = { noremap = true, buffer = buf }
	options = vim.tbl_extend("force", options, opts)

	map(mode, lhs, rhs, options)
end

return 1
