local this = {}

this.bootstrap = function()
	local lazypath = vim.fn.stdpath("data").."/lazy/lazy.nvim"
	if not vim.loop.fs_stat(lazypath) then
		vim.fn.system({
			'git',
			'clone',
			'--filter=blob:none',
			'git@github.com:folke/lazy.nvim.git',
			'--branch=stable',
			lazypath,
		})
	end
	vim.opt.rtp:prepend(lazypath)
end

this.run = function ()
	require('lazy').setup('plugs', {
		lockfile = vim.fn.stdpath("data").."lazy/lazy-lock.json",
		install = {
			missing = false,
		},
		change_detection = {
			notify = false,
		}
	})
end

return this
