if vim.env.EDITORPAGER == nil then return end

require('map')

local gp = vim.api.nvim_create_augroup('pagerSettings', {clear = true})

vim.opt.laststatus=0

vim.api.nvim_create_autocmd({'BufEnter'}, {
	callback = function()
		vim.bo.readonly=true
		vim.bo.modifiable = false
		vim.bo.modified = false
	end,
	group = gp,
})

noremap('n', 'q', ':q!<CR>')


