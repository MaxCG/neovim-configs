local gp = vim.api.nvim_create_augroup('whiteSpaceTrimmer', {clear = true})

vim.api.nvim_create_autocmd({'BufWritePre'},
	{ callback = function()
		local save = vim.fn.winsaveview()
		vim.api.nvim_command([[keeppatterns silent %s/\\\@<!\s\+$//e]])
		vim.fn.winrestview(save)
	end, group = gp})
