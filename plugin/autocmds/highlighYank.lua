local gp = vim.api.nvim_create_augroup("HighLightYank", { clear = true })

vim.g.onHighlightTimeout = 500

vim.api.nvim_create_autocmd({"TextYankPost"}, {
	callback = function ()
		require('vim.highlight').on_yank({timeout = vim.g.onHighlightTimeout})
	end,
	group = gp,
	desc = "Hightlight Yanked text"
})
