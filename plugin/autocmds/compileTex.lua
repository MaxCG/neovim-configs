local gp = vim.api.nvim_create_augroup('compileTex', {clear = true})

function watchTex()
	vim.api.nvim_create_autocmd(
		{'BufWritePost'},
		{
			callback = function()
				vim.fn.jobstart('PWD='.. vim.fn.expand('%:p:h') ..' pdflatex --interaction=nonstopmode -shell-escape -halt-on-error '..vim.fn.expand('%:p:t')..' > /dev/null', {detach = true})
			end,
			group = gp, pattern = '*.tex'
		}
	)
end

vim.api.nvim_create_user_command('WatchTex', watchTex, {})
