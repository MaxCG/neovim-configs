if vim.env.EDITORPAGER ~= nil then return end
local gp = vim.api.nvim_create_augroup('relativeNumberToggle', {clear = true})

vim.opt.number = true

vim.api.nvim_create_autocmd({'BufEnter', 'FocusGained', 'InsertLeave'}, 
	{ callback = function() vim.opt.relativenumber = true end, group = gp})
vim.api.nvim_create_autocmd({'BufLeave', 'FocusLost', 'InsertEnter'}, 
	{ callback = function() vim.opt.relativenumber = false end, group = gp})
