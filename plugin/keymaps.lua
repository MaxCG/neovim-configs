require('map')

-- basic colemak

local colemak = {
	{'H', '0'},
	{'n', 'j'},
	{'N', '<C-D>zz'},
	{'e', 'k'},
	{'E', '<C-U>zz'},
	{'i', 'l'},
	{'I', '$'},
	{'l', 'i'},
	{'L', 'I'},
	{'k', 'n'},
	{'K', 'N'},
	{'j', 'e'},
	{'J', 'E'},
	{'M', 'K'}
}

for _,m in ipairs(colemak) do
	noremap({'n', 'x', 'o'}, m[1], m[2])
end

-- indent

noremap('n', '<', 'v<')
noremap('n', '>', 'v>')
noremap('x', '<', '<gv')
noremap('x', '>', '>gv')

-- Pannel Movement

noremap('n', '<leader>h', '<C-W><C-H>')
noremap('n', '<leader>n', '<C-W><C-J>')
noremap('n', '<leader>e', '<C-W><C-K>')
noremap('n', '<leader>i', '<C-W><C-L>')

-- Normal funcs

noremap('n', ';', 'A;<ESC>')
noremap('n', 'C', 'ciw')
noremap('n', 'X', 'daw')
noremap('n', 'U', '<C-r>')
noremap({'n', 'x', 'o'}, 'Q', '<CMD>q<CR>')
noremap({'n', 'x', 'o'}, 'W', '<CMD>silent w | echo "svd" @%<CR>')

-- Copy and Paste

noremap({'n', 'x', 'o'}, '<C-y>', '"+y')
noremap({'n', 'x', 'o'}, '<C-p>', '"+p')
noremap({'n', 'x', 'o'}, '<C-d>', '"+d')

-- Arrow movements

noremap({'n', 'x', 'o'}, '<UP>', 'gk')
noremap({'n', 'x', 'o'}, '<DOWN>', 'gj')
noremap('i', '<UP>', '<C-o>gk')
noremap('i', '<DOWN>', '<C-o>gj')

-- Move lines

noremap('n', '<M-n>', '<CMD>m .+1<CR>')
noremap('n', '<M-e>', '<CMD>m .-2<CR>')
noremap('n', '<M-S-n>', '<CMD>t .<CR>')
noremap('n', '<M-S-e>', '<CMD>t .-1<CR>')

noremap('x', '<M-n>', ':m \'>+1 | echo<CR>gv')
noremap('x', '<M-e>', ':m \'<-2 | echo<CR>gv')
noremap('x', '<M-S-n>', ':t \'>. | echo<CR>gv')
noremap('x', '<M-S-e>', ':t \'<-1 | echo<CR>gv')

-- Exact mark jumping
noremap({'n', 'x', 'o'}, '\'' , '`')

