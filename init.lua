-- Map leader first

vim.g.mapleader = ' '

-- Helper funcs

require('map')

function req(m)
	local ok, pk = pcall(require, m)

	if not ok then
		print('Failed to require: '..m)
		return nil
	end

	return pk
end

function mergeTables(a, b)
	local c = {}

	for k, v in pairs(a) do
		c[k] = v
	end

	for k, v in pairs(b) do
		c[k] = v
	end

	return c
end

function inspect(a)
	print(vim.inspect(a))
end

-- Plugin setup

pluginManager = require('pluginManager')
pluginManager.bootstrap()
pluginManager.run()

-- Misc

vim.opt.mouse = 'a'
vim.opt.hlsearch = false

-- Indent

vim.opt.expandtab = false
vim.opt.copyindent = true
vim.opt.preserveindent = true
vim.opt.softtabstop = 0
vim.opt.shiftwidth = 3
vim.opt.tabstop = 3

-- Visual spaces

vim.opt.list = true
vim.opt.listchars = { tab = '  ', trail = '~', eol = ' ', extends = ' ' }
vim.opt.linebreak = true
vim.opt.showbreak = '>==>'
vim.opt.scrolloff = 5

-- Command Options

vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.inccommand = 'nosplit'

-- Spell checking

vim.opt.spell = true
vim.opt.spelllang = { 'pt_br', 'en_us' }
vim.api.nvim_set_hl(0, 'SpellBad', {italic = true, bold = true})
noremap({'n'}, '<leader>sc', function()
	vim.api.nvim_set_hl(0, 'SpellBad', {fg = '#FF0000',italic = true, bold = true})
end)
vim.api.nvim_set_hl(0, 'SpellRare', {})
vim.api.nvim_set_hl(0, 'SpellLocal', {})

